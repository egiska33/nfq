<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::resource('products', 'ProductsController');
    Route::get('addToCart/{id}', 'AddToCartController@add')->name('products.addToCart');
    Route::get('cart', 'AddToCartController@getCart')->name('getCart');
    Route::get('/remove/{id}', 'AddToCartController@getRemoveItem')->name('product.remove');
    Route::get('/reduce/{id}', 'AddToCartController@getReduceByOne')->name('product.reduceByOne');
    Route::get('/checkout', 'AddToCartController@postCheckout')->name('checkout.post');
    Route::get('user/{id}', 'UserController@getProfile')->name('user.profile');
    Route::get('user', 'UserController@getAllCheckout')->name('all.profile');

});