@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if (session('message'))
                <div class="alert alert-info">{{ session('message') }}</div>
            @endif
        </div>
        <div class="row">
            @can('create', \App\Product::class)
                <div class="col-md-12">
                    <a href="{{route('products.create')}}" class="btn btn-primary">Prideti</a>
                </div>
            @endcan

        </div>
        <div class="row">
            @forelse($products as $product)
                <div class=" col-md-4 card">
                    <div class="card__inner" style="background: url('/storage/image/{{$product->photo}}') no-repeat;background-size: 100%">
                        <h2>{{ $product->item }} <small>Series</small></h2>
                        <div class="card__buttons">
                            <a href="{{route('products.show', $product->id)}}">Info</a>
                            <a href="{{route('products.addToCart', $product->id)}}">Shop</a>
                        </div>
                    </div>
                    <h3 class="card__tagline">{{ $product->price }}</h3>
                    <div>
                        @can('update', \App\Product::class)
                            <a href="{{ route('products.edit', $product->id) }}" class="btn btn-default">Redaguoti</a>
                        @endcan

                        @can('delete', \App\Product::class)
                            <form action="{{ route('products.destroy', $product->id) }}" method="POST"
                                  style="display: inline"
                                  onsubmit="return confirm('Are you sure?');">
                                <input type="hidden" name="_method" value="DELETE">
                                {{ csrf_field() }}
                                <button class="btn btn-danger">Istrinti</button>
                            </form>
                        @endcan
                    </div>
                </div>
            @empty
                <div class="col-md-12">
                    <h3>No entries found.</h3>
                </div>
            @endforelse

        </div>

        {{ $products->links() }}
    </div>




@endsection