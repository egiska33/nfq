@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-primary">

                <div class="panel-heading">
                    {{$product->item}}
                </div>


                <div class="panel-body">
                    <div class="col-md-6">
                        {{$product->description}}
                    </div>

                    <div class="col-md-6" style="background: url('/storage/image/{{$product->photo}}') no-repeat;background-size: 100%; height: 300px">

                    </div>
                    <div class="col-md-3">
                        <div class="input-group form-param">
                            <span class="input-group-addon">Kaina</span>
                            <input id="importe" name="importe"
                                   class="form-control" placeholder="importe" type="text"
                                   disabled="disabled" value="{{$product->price}}">
                            <span class="input-group-addon" id="eur-addon">EUR</span>
                        </div><!-- .input-group -->
                    </div><!-- .col-md-3 -->


                </div><!-- .panel-body -->

                <div class="panel-footer">
                    <a class="btn-primary btn" href="{{route('products.index')}}">Atgal</a>
                </div>

            </div><!-- .panel -->

        </div>
    </div>

    @endsection