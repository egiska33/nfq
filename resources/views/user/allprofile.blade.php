@extends('layouts.app')

@section('content')
    <div id="cart">
        <div class="container align1">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1>User Profile</h1>
                    <hr>
                    <h2> Orders list</h2>
                    @foreach($orders as $order)
                        <div class="panel panel-default">
                            <div class="panel-footer">
                                <strong>{{$order->user->name}}</strong>
                                <br>
                                <strong>Date: {{$order->created_at}}</strong>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    @foreach($order->cart->items as $item)
                                        <li class="list-group-item">
                                            <span class="badge">{{$item['price']}} eur</span>
                                            {{ $item['item']['item'] }} | {{ $item['qty'] }} Units
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="panel-footer">
                                <strong>Total Price: {{$order->cart->totalPrice}}</strong>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection