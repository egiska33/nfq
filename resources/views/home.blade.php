@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class=" col-md-4 card">
            <div class="card__inner" style="background: url('https://unsplash.it/400?random') no-repeat">
                <h2>Defender <small>Series</small></h2>
                <div class="card__buttons">
                    <a href="#">Explore</a>
                    <a href="#">Shop</a>
                </div>
            </div>
            <h3 class="card__tagline">Kaina</h3>
        </div>
        <div class=" col-md-4 card">
            <div class="card__inner" style="background: url('https://unsplash.it/400?random') no-repeat">
                <h2>Defender <small>Series</small></h2>
                <div class="card__buttons">
                    <a href="#">Explore</a>
                    <a href="#">Shop</a>
                </div>
            </div>
            <h3 class="card__tagline">Kaina</h3>
        </div>
        <div class=" col-md-4 card">
            <div class="card__inner" style="background: url('https://unsplash.it/400?random') no-repeat">
                <h2>Defender <small>Series</small></h2>
                <div class="card__buttons">
                    <a href="#">Explore</a>
                    <a href="#">Shop</a>
                </div>
            </div>
            <h3 class="card__tagline">Kaina</h3>
        </div>
    </div>
</div>
@endsection
