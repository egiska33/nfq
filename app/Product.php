<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use softDeletes ;

    protected $fillable = ['item', 'photo', 'description', 'price'];

    protected $dates = ['deleted_at'];
}
