<?php

namespace App;


class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    /**
     * Cart constructor.
     * @param $oldCart
     */
    public function  __construct($oldCart)
    {
        if($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    /**
     * @param $item
     * @param $id
     */
    public function add($item, $id)
    {
        $storeItem = ['qty'=> 0, 'price'=>$item->price, 'item'=>$item];
        if($this->items) {
            if(array_key_exists($id, $this->items)) {
                $storeItem = $this->items[$item->id];
            }
        }
        $storeItem['qty']++;
        $storeItem['price'] = $item->price * $storeItem['qty'];
        $this->items[$item->id] = $storeItem;
        $this->totalQty++;
        $this->totalPrice += $item->price;
    }

    /**
     * @param $id
     */
    public function removeByOne($id)
    {
        $this->items[$id]['qty']--;
        $this->items[$id]['price']-= $this->items[$id]['item']['price'];
        $this->totalQty--;
        $this->totalPrice -= $this->items[$id]['item']['price'];

        if($this->items[$id]['qty'] <= 0) {
            unset($this->items[$id]);
        }
    }

    /**
     * @param $id
     */
    public function removeItem($id)
    {
        $this->totalQty -= $this->items[$id]['qty'];
        $this->totalPrice -= $this->items[$id]['price'];

        unset($this->items[$id]);
    }
}
