<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile()
    {
        $orders = Auth::user()->orders;
        $orders->transform(function ($order){
            $order->cart = unserialize($order->cart);
            return $order;
        });
        return view('user.profile', compact('orders'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllCheckout()
    {
        $orders = Order::orderBy('user_id', 'asc')->get();
        $orders->transform(function ($order){
            $order->cart = unserialize($order->cart);
            return $order;
        });
        return view('user.allprofile', compact('orders'));
    }
}
